import React, {useState} from 'react';

export default function ({defaultName}:{defaultName:string}) {
    const [name, setName] = useState(defaultName)

    return <input data-testid="edit-name-input" value={name} onChange={event => setName(event.target.value)}/>
}
