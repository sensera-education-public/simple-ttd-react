import React from 'react';

export default function ({name}:{name:string}) {
    return <h2 data-testid="view-name-input">{name}</h2>
}
