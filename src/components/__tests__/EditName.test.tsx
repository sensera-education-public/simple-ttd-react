import React from 'react';
import {fireEvent, render, screen} from '@testing-library/react';
import EditName from "../EditName";

test('test render EditName correct', () => {
    render(<EditName defaultName="Arne"/>);

    const linkElement = screen.getByTestId("edit-name-input");
    expect(linkElement).toBeInTheDocument();
    expect(linkElement).toBeVisible();
    expect(linkElement).toHaveAttribute("value","Arne");
});

test('test render EditName correct when input change', () => {
    render(<EditName defaultName="Arne"/>);

    const linkElement = screen.getByTestId("edit-name-input");

    fireEvent.change(linkElement,{target:{value:"Gullan"}})

    expect(linkElement).toBeInTheDocument();
    expect(linkElement).toBeVisible();
    expect(linkElement).toHaveAttribute("value","Gullan");
});
