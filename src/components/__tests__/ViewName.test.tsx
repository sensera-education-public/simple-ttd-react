import React from 'react';
import { render, screen } from '@testing-library/react';
import ViewName from "../ViewName";

test('test render ViewName correct', () => {
    render(<ViewName name="Arne"/>);
    const linkElement = screen.getByTestId("view-name-input");
    expect(linkElement).toBeInTheDocument();
    expect(linkElement).toBeVisible();
    expect(linkElement).toMatchSnapshot();
});
